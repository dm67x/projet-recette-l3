# Projet Licence 3 - Recette

## Technologies utilisées

* NodeJS (JavaScript) avec le framework ExpressJS
* Sequelize pour l'ORM
* Bulma pour le framework CSS
* Bcrpypt pour hasher le mot de passe
* Gestion des modals par un script perso

## Comment lancer le serveur

* Télécharger NodeJS et PostgreSQL
* Aller sur ce repertoire et faire un "npm install"
* Puis modifier le fichier configuration dans config/.config.json en mettant ses identifiants de connexion à PostgreSQL (partie development)
* Enfin faire "npm start" et aller sur localhost:3000

## Taches

* [x] Création, modification et suppression recettes
* [x] Création, modification et suppression d'ingrédients
* [x] Création, modification et suppression des unités
* [x] Création, modification et suppression d'utilisateurs
* [x] Connexion, Inscription et Déconnexion (mot de passe crypté avec bcrypt)
* [x] Gestion plannings
* [x] Gestion de quelques erreurs (pas toutes, manque de temps)
* [x] Visualisation des recettes
* [x] Recherche des recettes par ingrédients
* [x] Gestion de l'administration