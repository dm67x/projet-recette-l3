var models = require('../models');
var Sequelize = require('sequelize');

// Models utilisés dans ce controleur
var Ingredient = models.Ingredient;
var Utilisateur = models.Utilisateur;
var Recette = models.Recette;
var Unite = models.Unite;
var Difficulte = models.Difficulte;

const AdminsController = new Object();

/* Index - GET */
AdminsController.index = function (req, res, next) {
    if (!req.session.admin) return next();
    return res.render('pages/admin/index', { user: req.session.login });
};

/* Afficher tous les ingrédients */
AdminsController.ingredients = function (req, res, next) {
    if (!req.session.admin) return next();
    Ingredient.all().then(function (ingredients) {
        return res.render('pages/admin/ingredients', { 
            ingredients, 
            user: req.session.login 
        });
    }).error(function (err) {
        return next();
    });
};

/* Creer un ingredient */
AdminsController.create_ingredient = function (req, res, next) {
    if (req.method != "POST" || !req.session.admin) return next();
    var body = req.body;

    Ingredient.create({
        nom: body.nom,
        popularite: body.popularite,
        qte_protides: body.qte_protides,
        qte_lipides: body.qte_lipides,
        qte_calories: body.qte_calories,
        qte_glucides: body.qte_glucides,
        UniteId: req.body.UniteId
    }).then(function () {
        return res.send(JSON.stringify({ 
            success: true, 
            redirect: 'ingredients' 
        }));
    })
    .catch(Sequelize.ValidationError, function (err) {
        return res.send(JSON.stringify({ success: false, errors: err }));
    })
    .error(function (err) {
        return res.send(JSON.stringify({ success: false, errors: err }));
    });
};

/* Supprimer un ingrédient */
AdminsController.remove_ingredient = function (req, res, next) {
    if (!req.query.id || !req.session.admin) return next();
    Ingredient.destroy({
        where: { id: req.query.id }
    }).then(function () {
        return res.redirect('ingredients');
    }).error(function (err) {
        return next();
    });
};

/* Modifier un ingrédient */
AdminsController.update_ingredient = function (req, res, next) {
    if (!req.query.id || !req.session.admin) return next();
    if (req.method == "GET") {      
        Ingredient.find({
            where: { id: req.query.id }
        }).then(function (ingr) {
            if (ingr) {
                return res.render("pages/admin/update_ingredient", { 
                    ingredient: ingr, 
                    user: req.session.login 
                });
            } else {
                return res.send({ 
                    success: false, 
                    error: "L'ingrédient n'existe pas" 
                });
            }
        });
    }

    Ingredient.find({
        where: { id: req.query.id }
    }).then(function (ingr) {
        if (ingr) {
            ingr.updateAttributes({
                nom: req.body.nom,
                qte_protides: req.body.qte_protides,
                qte_lipides: req.body.qte_lipides,
                qte_calories: req.body.qte_calories,
                qte_glucides: req.body.qte_glucides
            }).then(function () {
                return res.redirect('ingredients');
            })
            .catch(Sequelize.ValidationError, function (err) {
                return res.send({ success: false, error: err });  
            });
        } else {
            return res.send({ 
                success: false, 
                error: "L'ingrédient n'existe pas" 
            });
        }
    });
};

/* Afficher tous les utilisateurs */
AdminsController.users = function (req, res, next) {
    if (!req.session.admin) return next();
    Utilisateur.all()
    .then(function (users) {
        return res.render('pages/admin/utilisateurs', { 
            user: req.session.login, 
            users 
        });
    })
    .error(function (err) {
        return next();
    });
};

/* Supprimer un utilisateur */
AdminsController.remove_user = function (req, res, next) {
    if (!req.session.admin || !req.query.login) return next();
    Utilisateur.destroy({
        where: { login: req.query.login }
    }).then(function () {
        return res.redirect('users');
    });
};

/* Modifier un utilisateur */
AdminsController.update_user = async function (req, res, next) {
    if (!req.session.admin || !req.query.login) return next();
    if (req.method == "GET") {
        Utilisateur.find({
            where: { login: req.query.login }
        }).then(function (u) {
            if (u) {
                return res.render('pages/admin/update_utilisateur', { 
                    user: req.session.login, 
                    user: u 
                });
            }

            return res.send(JSON.stringify({ 
                success: false, 
                error: "L'utilisateur n'existe pas" 
            }));
        });
    }

    const user = await Utilisateur.update_user(req.query.login, "", 
        req.body.email, req.body.nom, req.body.prenom, req.body.adresse);

    if (user.success) {
        return res.redirect('users');
    }
    return res.send(JSON.stringify(user));
};

/* Afficher toutes les recettes */
AdminsController.recettes = function (req, res, next) {
    if (!req.session.admin) return next();
    Recette.all({
        include: [ models.Utilisateur, models.Difficulte ]
    })
    .then(function (recettes) {
        return res.render('pages/admin/recettes', { 
            user: req.session.login, 
            recettes 
        });
    })
    .error(function (err) {
        return next();
    });
};

/* Supprimer une recette */
AdminsController.remove_recette = function (req, res, next) {
    if (!req.session.admin || !req.query.id) return next();
    Recette.destroy({
        where: { id: req.query.id }
    }).then(function () {
        return res.redirect('recettes');
    });
};

/* Modifier une recette */
AdminsController.update_recette = async function (req, res, next) {
    if (!req.query.id || !req.session.admin) return next();
    if (req.method == "GET") {
        const difficultes = await Difficulte.all();
        const recette = await Recette.findById(req.query.id);
        if (recette) {
            return res.render('pages/admin/update_recette', { 
                user: req.session.login,
                recette,
                difficultes
            });
        } else {
            return res.redirect('recettes');
        }
    }

    const recette = await Recette.update_recette(models, req.query.id, 
        {
            nom: req.body.nom, 
            description: req.body.description, 
            nbre_personnes: req.body.nbre_personnes, 
            prix: req.body.prix, 
            DifficulteId: req.body.difficulte
        }
    );

    if (recette.success) {
        return res.redirect('recettes');
    } 

    if (recette.error.name == "SequelizeValidationError") {
        recette.error.errors.forEach(function (error) {
            req.flash('error', error.message);
        });
        return res.redirect('update_recette');
    }

    return res.send(JSON.stringify(recette));
};

/* Afficher toutes les unites */
AdminsController.unites = function (req, res, next) {
    if (!req.session.admin) return next();
    Unite.all().then(function (unites) {
        return res.render('pages/admin/unites', { 
            user: req.session.login, 
            unites 
        });
    });
};

/* Supprimer une unité */
AdminsController.remove_unite = function (req, res, next) {
    if (!req.query.id || !req.session.admin) return next();
    Unite.destroy({
        where: { id: req.query.id }
    }).then(function () {
        return res.redirect('unites');
    });
};

/* Creer une unité */
AdminsController.create_unite = function (req, res, next) {
    if (!req.body || !req.session.admin) return next();
    Unite.create(req.body).then(function () {
        return res.send(JSON.stringify({ 
            success: true, 
            redirect: 'unites' 
        }));
    }).error(function (err) {
        return res.send(JSON.stringify({ success: false, errors: err }));
    });
};

module.exports = AdminsController;