var fs = require('fs');
var path = require('path');

var controllers = {};

var files = fs.readdirSync(__dirname);
files.forEach(file => {
	if (file != "index.js") {
		var name = file.split("_controller")[0];
		controllers[name] = require(path.resolve(__dirname, file));
	}
});

module.exports = controllers;