var models = require('../models');
var Sequelize = require('sequelize');

// Models utilisés dans ce controleur
var Planning = models.Planning;
var Utilisateur = models.Utilisateur;
var Recette = models.Recette;

const PlanningsController = {};

/* Index - GET */
PlanningsController.index = function (req, res, next) {
    if (!req.session.login) return next();

    Planning.all({
        include: [
            {
                model: Utilisateur,
                where: { login: req.session.login }
            },
            Recette
        ]
    }).then(function (plannings) {
        return res.render('pages/plannings/index', { plannings });
    }).error(function (err) {
        return next();
    });
};

/* Ajouter le menu au planning */
PlanningsController.add_menu = function (req, res, next) {
    if (!req.session.login) return next();

    // Verification arguments
    if (!req.query.date || !req.query.horaire) {
        return res.redirect('/r/show?id=' + req.query.rid);
    }

    // Creer si n'existe pas
    Planning.addMenu(
        models, 
        req.query.date, 
        req.query.horaire, 
        req.query.rid, 
        req.session.login
    ).then(function (planning) {
        if (planning.success) return res.redirect('index');
        return res.redirect('/r/show?id=' + req.query.rid);
    });
};

module.exports = PlanningsController;