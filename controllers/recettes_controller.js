var models = require('../models');
var Sequelize = require('sequelize');

// Models utilisés dans ce controleur
var Recette = models.Recette;
var Difficulte = models.Difficulte;
var Etape = models.Etape;
var Utilisateur = models.Utilisateur;
var Ingredient = models.Ingredient;
var Unite = models.Unite;

const RecettesController = new Object();

/* Visualiser toutes les recettes */
RecettesController.index = function (req, res, next) {
	Recette.all({
		include: [ Utilisateur, Difficulte ]
	})
	.then(function (recettes) {
		return res.render('pages/recettes/index', { recettes: recettes });
	})
	.error(function(err) {
		return next();
	});
};

/* Creer une recette */
RecettesController.create = function (req, res, next) {
	var nom = req.body.nom;
	var description = req.body.description;
	var nb_pers = req.body.nbre_personnes;
	var prix = req.body.prix;
	var ingredients = JSON.parse(req.body.ingredients); // { id, qte }
	var qte_calories = req.body.qte_calories; // js
	var qte_protides = req.body.qte_protides; // js
	var qte_lipides = req.body.qte_lipides; // js
	var qte_glucides = req.body.qte_glucides; // js
	var duree = req.body.duree; // update en javascript
	var etapes = JSON.parse(req.body.etapes); // { nom, description, duree, position }
	var difficulte = req.body.difficulte;

	// Si pas connecté
	if (!req.session.login) return res.redirect('/');

	Recette.add_recipe(models, {
		nom,
		description,
		nbre_personnes: nb_pers,
		prix,
		qte_calories,
		qte_glucides,
		qte_lipides,
		qte_protides,
		duree,
		DifficulteId: difficulte,
		UtilisateurLogin: req.session.login
	}, ingredients, etapes)
	.then(function (recette) {
		if (recette.success) {
			return res.redirect('/r');
		}

		if (recette.error.name == "SequelizeValidationError") {
			recette.error.errors.forEach(function (error) {
				req.flash('error', error.message);
			});
			return res.redirect('new');
		}
		return res.send(JSON.stringify(recette));
	});
};

/* Creer une recette, page formulaire */
RecettesController.new = function (req, res, next) {
	var promises = [];
	if (!req.session.login) return res.redirect('/');
	promises.push(Ingredient.all({ include: [ Unite ]}));
	promises.push(Difficulte.all());

	Promise.all(promises).then(function (values) {
		return res.render('pages/recettes/new', { 
			ingredients: values[0], 
			difficultes: values[1]
		});
	}).catch(function (err) {
		return next();
	});
};

/* Visualiser une recette */
RecettesController.show = async function (req, res, next) {
	const recette = await Recette.findById(req.query.id, {
		include: [
			{
				model: Etape, as: "Etapes"
			},
			{
				model: Utilisateur, as: "Utilisateur"
			},
			{
				model: Ingredient,
				include: [ Unite ]
			},
			Difficulte
		]
	});

	if (recette) {
		return res.render('pages/recettes/show', { 
			recette: recette, 
			ingredients: recette.Ingredients,
			etapes: recette.Etapes,
			user: recette.Utilisateur
		});
	}

	return res.redirect('/');
};

/* Trouver une recette */
RecettesController.find = function (req, res, next) {
	Recette.find_recipe(models, req.query.q)
	.then(function (recettes) {
		return res.render('pages/recettes/resultat', { recettes });
	})
	.error(function (err) {
		return next();	
	});
};

module.exports = RecettesController;