var models = require('../models');

// Models utilisés dans ce controleur
var Unite = models.Unite;

const UnitesController = new Object();

/* Recuperer toutes les unites */
UnitesController.all = function (req, res, next) {
    Unite.all()
    .then(function (unites) {
        return res.send(JSON.stringify(unites));
    })
    .error(function (err) {
        return next();
    });
};

module.exports = UnitesController;