var models = require('../models');

// Models utilisés dans ce controleur
var Utilisateur = models.Utilisateur;

const UsersController = new Object();

/* Creer un utilisateur */
UsersController.create = async function (req, res, next) {
	if (req.session.login) return res.redirect('dashboard');
	// Creer un compte utilisateur
	var login = req.body.login;
	var email = req.body.email;
	var nom = req.body.nom;
	var prenom = req.body.prenom;
	var adresse = req.body.adresse;
	var mdp = req.body.mdp;

	const user = await Utilisateur.add_user(
		login, mdp, email, nom, prenom, adresse
	);

	if (user.success) {
		req.session.login = user.user.login;
		return res.send(JSON.stringify({ redirect: "/u/dashboard" }));
	}

	return res.send(JSON.stringify(user));
};

/* Deconnexion */
UsersController.signout = function (req, res, next) {
	if (req.session.login) req.session.login = null;
	return res.redirect('/'); 
};

/* Se connecter */
UsersController.signin = async function (req, res, next) {
	if (req.session.login) return res.redirect('dashboard');
	var mdp = req.body.mdp;
	var login = req.body.login;

	const user = await Utilisateur.sign_in(login, mdp);
	if (user.success) {
		req.session.login = user.user.login;
		return res.send(JSON.stringify({ redirect: "/u/dashboard" }));
	}

	return res.send(JSON.stringify(user));
};

/* Tableau de bord */
UsersController.dashboard = function (req, res, next) {
	if (!req.session.login) return res.redirect('/');
	Utilisateur.findOne({
		where: { 
			login: req.session.login
		}
	})
	.then(function (user) {
		return res.render('pages/users/dashboard', { user: user });
	})
	.error(function(err) {
		return next();
	});
};

/* Modifier mes informations */
UsersController.update = async function (req, res, next) {
	if (!req.session.login && req.request != "POST") return res.redirect('/');
	const user = await Utilisateur.update_user(
		req.body.login,
		req.body.mdp,
		req.body.email,
		req.body.nom,
		req.body.prenom,
		req.body.adresse
	);

	if (user) {
		req.session.login = user.user.login;
		return res.redirect('dashboard');
	}

	return res.send(JSON.stringify(user));
};

module.exports = UsersController;