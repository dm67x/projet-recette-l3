"use strict";

module.exports = function (sequelize, DataTypes) {

    var contient_ingredient = sequelize.define("Contient_Ingredient", {
        qte: {
            type: DataTypes.INTEGER,
            notNull: true,
            min: 1
        }
    });

    contient_ingredient.associate = function (models) {
        contient_ingredient.belongsTo(models.Recette, { onDelete: 'cascade', hooks: true });
        contient_ingredient.belongsTo(models.Ingredient, { onDelete: 'cascade', hooks: true });
    };

    return contient_ingredient;

};