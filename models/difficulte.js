"use strict";

module.exports = function (sequelize, DataTypes) {

    var difficulte = sequelize.define("Difficulte", {
        label: {
            type: DataTypes.STRING,
            allowNull: false
        }
    });

    difficulte.associate = function (models) {
        difficulte.hasMany(models.Recette);
    }

    return difficulte;

}