"use strict";

module.exports = function (sequelize, DataTypes) {

    var dispose = sequelize.define("Dispose");

    dispose.associate = function (models) {
        dispose.belongsTo(models.Utilisateur);
        dispose.belongsTo(models.Ingredient);
    };

    return dispose;

};