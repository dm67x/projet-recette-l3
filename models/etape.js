"use strict";

module.exports = function (sequelize, DataTypes) {

    var etape = sequelize.define("Etape", {
        nom: {
            type: DataTypes.STRING,
            allowNull: false
        },
        description: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        duree: {
            type: DataTypes.INTEGER,
            allowNull: false,
            isNumeric: true,
            min: 1
        },
        position: {
            type: DataTypes.INTEGER,
            allowNull: false
        }
    });

    etape.associate = function (models) {
        etape.belongsTo(models.Recette, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            } 
        });
    };

    return etape;

}