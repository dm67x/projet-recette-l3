"use strict";

module.exports = function (sequelize, DataTypes) {

    var ingredient = sequelize.define("Ingredient", {
        nom: {
            type: DataTypes.STRING,
            allowNull: false
        },
        qte_lipides: {
            type: DataTypes.INTEGER,
            min: 0
        },
        qte_glucides: {
            type: DataTypes.INTEGER,
            min: 0
        },
        qte_protides: {
            type: DataTypes.INTEGER,
            min: 0
        },
        qte_calories: {
            type: DataTypes.INTEGER,
            min: 0
        },
        popularite: DataTypes.INTEGER
    });

    ingredient.associate = function (models) {
        ingredient.belongsToMany(models.Recette, { through: models.Contient_Ingredient });
        //ingredient.belongsToMany(models.Planning, { through: models.Ingredient_A_Acheter });
        ingredient.belongsToMany(models.RegimeAlimentaire, { through: 'InterditDans' });
        ingredient.belongsTo(models.Unite, {
            onDelete: 'cascade',
            foreignKey: { 
                fieldName: 'UniteId',
                allowNull: false
            }
        });
    };

    return ingredient;

};