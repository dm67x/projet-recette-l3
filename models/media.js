"use strict";

module.exports = function (sequelize, DataTypes) {

    var media = sequelize.define("Media", {
        type: DataTypes.BOOLEAN,
        lien: DataTypes.STRING
    });

    media.associate = function (models) {
        media.belongsTo(models.Recette);
    };

    return media;

};