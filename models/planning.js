"use strict";

module.exports = function (sequelize, DataTypes) {

    var planning = sequelize.define("Planning", {
        horaire: {
            type: DataTypes.STRING,
            validate: {
                isIn: [["matin", "midi", "soir"]]
            },
            primaryKey: true
        },
        date: {
            type: DataTypes.DATE,
            primaryKey: true,
            validate: {
                isAfter: sequelize.fn('NOW')
            }
        }
    });

    planning.associate = function (models) {
        planning.belongsTo(models.Utilisateur, { 
            foreignKey: {
                primaryKey: true 
            },
            onDelete: 'cascade', 
            hooks: true 
        });

        planning.belongsTo(models.Recette, {
            foreignKey: {
                primaryKey: true
            },
            onDelete: "CASCADE", 
            hooks: true 
        });
    };

    planning.addMenu = function (models, date, horaire, recette, user) {
        return planning.findCreateFind({
            where: {
                horaire: horaire,
                date: date,
                RecetteId: recette,
                UtilisateurLogin: user
            },
            defaults: {
                date: date,
                horaire: horaire,
                RecetteId: recette,
                UtilisateurLogin: user
            }
        }).spread(function (planning, created) {
            return { success: true, planning: planning };
        }).catch(sequelize.ValidationError, function (err) {
            return { success: false, error: err };
        }).error(function (err) {
            return { success: false, error: err };
        });
    };

    return planning;

};