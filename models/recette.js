"use strict";

module.exports = function (sequelize, DataTypes) {

    var recette = sequelize.define("Recette", {
        nom: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        description: {
            type: DataTypes.TEXT,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        nbre_personnes: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                isNumeric: true,
                min: 1
            }
        },
        prix: {
            type: DataTypes.INTEGER, // entre 1 et 5
            allowNull: false,
            validate: {
                isIn: [[1, 2, 3, 4, 5]],
            }
        },
        qte_lipides: {
            type: DataTypes.INTEGER,
            validate: {
                min: 0
            }
        },
        qte_glucides: {
            type: DataTypes.INTEGER,
            validate: {
                min: 0
            }
        },
        qte_protides: {
            type: DataTypes.INTEGER,
            validate: {
                min: 0
            }
        },
        qte_calories: {
            type: DataTypes.INTEGER,
            validate: {
                min: 0
            }
        },
        duree: {
            type: DataTypes.INTEGER,
            validate: {
                min: 1
            }
        } // en minutes
    });

    recette.associate = function (models) {
        recette.belongsTo(models.Difficulte, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false,
                validate: {
                    notEmpty: true,
                    isNumeric: true
                }
            }
        });

        recette.belongsTo(models.Utilisateur, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });

        recette.hasMany(models.Etape);
        recette.hasMany(models.Media);
        recette.belongsToMany(models.Ingredient, { through: models.Contient_Ingredient });
        recette.hasMany(models.Planning);
    };

    recette.add_recipe = function (models, object, ingredients, etapes) {
        return recette.create(
            Object.assign({}, object, { Etapes: etapes }), 
            { include: [ models.Etape ] }
        )
        .then(function (r) {
            if (r) {
                ingredients.forEach(function (ingredient) {
                    r.addIngredient(ingredient.id, { 
                        through: { 
							qte: ingredient.qte 
                        } 
                    })
                    .catch(sequelize.ValidationError, function (err) {
                        return { success: false, error: err };
                    });
                });

                return { success: true, recette: r };
            }
            return { success: false, error: "Erreur lors de la création de la recette" };
        })
        .catch(sequelize.ValidationError, function (err) {
            return { success: false, error: err };
        });
    };

    recette.update_recette = async function (models, id, object) {
        return recette.update(object, {
            where: { id: id }
        }).then(function (re) {
            if (re) {
                return { success: true, recette: re };
            } else {
                return { success: false, error: "La recette n'existe pas" };
            }
        })
        .catch(sequelize.ValidationError, function (err) {
            return { success: false, error: err };
        })
        .error(function (err) {
            return { success: false, error: err }; 
        });
    };

    recette.prototype.get_prix = function () {
        switch (this.prix) {
            case 1:
                return "Presque gratuit";
            case 2:
                return "Bon marché";
            case 3:
                return "Modéré";
            case 4:
                return "Cher";
            case 5:
                return "Très cher";
        }
    };

    recette.find_recipe = function (models, q) {
        var split = q.split(/[,|\s]/);
        split = split.filter(function(n){ return n != '' });
        
        var where_arguments = [];
        console.log(split);
        split.forEach(function (s) {
            where_arguments.push({ [sequelize.Op.iLike]: '%' + s + '%' });
        });

        return recette.findAll({
            include: [
                {
                    model: models.Ingredient,
                    where: {
                        nom: {
                            [sequelize.Op.or]: where_arguments
                        }
                    }
                },
                models.Utilisateur, 
                models.Difficulte
            ]
        });
    };

    return recette;

}