"use strict";

module.exports = function (sequelize, DataTypes) {

    var regime = sequelize.define("RegimeAlimentaire", {
        label: DataTypes.STRING
    });

    regime.associate = function (models) {
        regime.belongsToMany(models.Ingredient, { through: 'InterditDans' });
    }

    return regime;

};