"use strict";

module.exports = function (sequelize, DataTypes) {

    var unite = sequelize.define("Unite", {
        label: DataTypes.STRING,
        indice: DataTypes.STRING
    });

    unite.associate = function (models) {
        unite.hasMany(models.Ingredient);
    }

    unite.prototype.getIndice = function () {
        if (this.indice == "g") {
            return "g de "
        }
        return this.indice;
    }

    return unite;
    
};