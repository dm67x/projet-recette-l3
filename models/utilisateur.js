"use strict";

var bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = function (sequelize, DataTypes) {

    var user = sequelize.define("Utilisateur", {
        login: {
            primaryKey: true,
            type: DataTypes.STRING,
            allowNull: false
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                isEmail: true
            }
        },
        mdp: {
            type: DataTypes.STRING,
            allowNull: false
        },
        nom: DataTypes.STRING,
        prenom: DataTypes.STRING,
        adresse: DataTypes.STRING,
        admin: DataTypes.BOOLEAN
    });

    user.associate = function (models) {
        user.hasMany(models.Recette);
        user.hasMany(models.Planning);
    };

    user.prototype.getFullName = function () {
        if (this.prenom != "" && this.nom != "")
            return [this.prenom, this.nom].join(' ');
        return this.login;
    };

    user.prototype.isAdmin = function () {
        return this.admin;
    };

    /* Création compte */
    user.add_user = function (login, mdp, email, nom, prenom, adresse) {
        /* Verification des parametres */
        if (login == "" || mdp == "" || email == "") {
            return { success: false, error: "Un des champs est vide" };
        }
        
        /* Hash mdp */
        const hashed_mdp = bcrypt.hashSync(mdp, saltRounds);

        return user.create({
            login: login,
            email: email,
            mdp: hashed_mdp,
            nom: nom,
            prenom: prenom,
            adresse: adresse
        }).then(function (u) {
            if (u) {
                return { success: true, user: u };
            }

            return { 
                success: false, 
                error: "Une erreur s'est produite lors de la création de compte" 
            };
        })
        .catch(sequelize.ValidationError, function (err) {
            return { 
                success: false, 
                error: "Une erreur s'est produite lors de la création de compte" 
            };
        });
        
    };

    /* Connexion */
    user.sign_in = function (login, mdp) {
        /* find user with login */
        return user.findOne({ where: { login: login }})
        .then(function (u) {
            /* compare bdd hash mdp and current mdp */
            if (u) {
                const value = bcrypt.compareSync(mdp, u.mdp);
                if (value) return { success: true, user: u };
                return { success: false, error: "Le mot de passe ne correspond pas" };
            }

            return { success: false, error: "Le compte utilisateur n'existe pas" };
        })
        .catch(sequelize.ValidationError, function (err) {
            return { success: false, error: err };
        });
    };

    /* Modifier l'utilisateur */
    user.update_user = function (login, mdp, email, nom, prenom, adresse) {
        /* find user with login */
        return user.findOne({ 
            where: { login: login } 
        })
        .then(function (u) {
            if (u) {
                /* Crypté le nouveau mot de passe */
                var value = u.mdp;
                if (mdp != null && mdp != "") {
                    value = bcrypt.hashSync(mdp, saltRounds);
                }

                return u.updateAttributes({
                    login: login,
                    mdp: value,
                    email: email,
                    nom: nom,
                    prenom: prenom,
                    adresse: adresse
                })
                .then(function (updated_user) {
                    if (updated_user) return { success: true, user: updated_user };
                    return { success: false, error: "Problème est survenu lors de la modification des valeurs" };
                })
                .catch(sequelize.ValidationError, function (err) {
                    return { success: false, error: err };
                });
            }
            return { success: false, error: "Le compte utilisateur n'existe pas" };
        })
        .catch(sequelize.ValidationError, function (err) {
            return { success: false, error: err };
        });   
    };

    return user;

}