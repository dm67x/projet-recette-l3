$(document).ready(() => {
    // Modal signin
    var modalSignin = new Modal("#signin-modal");
    var modalSignup = new Modal("#signup-modal");

    $("#signin-link").click(function (event) {
        event.preventDefault();

        modalSignup.close();
        modalSignin.open();
    });

    $("#signin-btn").click(function (event) {
        event.preventDefault();

        $.ajax({
            type: "POST",
            url: "/u/signin",
            data: {
                login: $("#login").val(),
                mdp: $("#mdp").val()
            }
        }).done(function (data) {
            var res = JSON.parse(data);
            if (res.error) {
                swal({
                    title: "Erreurs",
                    text: res.error,
                    icon: "error"
                });
            }
            else window.location.replace(res.redirect);
        });
    });

    $("#signup-link").click(function (event) {
        event.preventDefault();

        modalSignin.close();
        modalSignup.open();
    });

    $("#signup-btn").click(function (event) {
        event.preventDefault();

        if ($("#mdp_i").val() != $("#cmdp_i").val()) {
            swal({
                title: "Erreur",
                text: "Les deux mots de passes sont différents",
                icon: "error"
            });
        }

        $.ajax({
            type: "POST",
            url: "/u/create",
            data: {
                login: $("#login_i").val(),
                mdp: $("#mdp_i").val(),
                email: $("#email_i").val(),
                nom: $("#nom_i").val(),
                prenom: $("#prenom_i").val(),
                adresse: $("#adresse_i").val()
            }
        }).done(function (data) {
            var res = JSON.parse(data);
            if (res.error) {
                swal({
                    title: "Erreurs",
                    text: res.error,
                    icon: "error"
                });
            }
            else window.location.replace(res.redirect);
        });
    });

    // Ajouter ingrédient dans la liste des recettes
    $("#addIngredient").click(function (event) {
        event.preventDefault();

        var ingre = JSON.parse($("#ingredient").val());
        var nom = $("#ingredient option:selected").text();
        var qte = $("#qte").val();

        var ingr = { id: ingre.id, qte };
        var current = JSON.parse($("#ingredients").val());
        current.push(ingr);
        $("#ingredients").val(JSON.stringify(current));

        var ingredient = document.createElement("div");
        var name = document.createElement("span");
        var quantity = document.createElement("p");

        $(name).text(nom + " : " + qte + " " + ingre.Unite.indice);
        $(ingredient).attr("class", "tag is-link").append(name);
        $(this).parent().find(".ingredient-zone").append(ingredient);
        
        $("#qte").val(0);

        /* Calcul quantité de calories.... */
        var qte_calories = parseInt($("#qte_calories").val());
        var qte_protides = parseInt($("#qte_protides").val());
        var qte_lipides = parseInt($("#qte_lipides").val());
        var qte_glucides = parseInt($("#qte_glucides").val());

        $("#qte_calories").val(qte_calories + ingre.qte_calories * qte);
        $("#qte_protides").val(qte_protides + ingre.qte_protides * qte);
        $("#qte_lipides").val(qte_lipides + ingre.qte_lipides * qte);
        $("#qte_glucides").val(qte_glucides + ingre.qte_glucides * qte);
    });

    // Ajouter étape dans la liste des recettes
    $("#addEtape").on("click", function() {
        var nom = $("#nome").val();
        var desc = $("#desce").val();
        var duree = parseInt($("#duree").val());

        var etapes = JSON.parse($("#etapes").val());
        var position = etapes.length + 1;
        var etape = { nom, description: desc, duree, position };
        etapes.push(etape);
        $("#etapes").val(JSON.stringify(etapes));

        var etapes_d = document.createElement("div");
        var name = document.createElement("span");

        $(name).text(nom + " " + duree + "min");
        $(etapes_d).attr("class", "tag is-link").append(name);
        $(this).parent().find(".etapes-zone").append(etapes_d);

        $("#duree_r").val(parseInt($("#duree_r").val()) + duree);

        $("#nome").val("");
        $("#desce").val("");
        $("#duree").val(0);
    });

    // Ajouter un ingrédient côté Admin
    var ingredientModal = new Modal("#ingredient-modal");
    
    $("#new-ingredient").click(function (event) {
        event.preventDefault();

        ingredientModal.open();
    });

    // Ajouter une unité côté admin
    var uniteModal = new Modal("#unite-modal");
    
    $("#new-unite").click(function (event) {
        event.preventDefault();

        uniteModal.open();
    });

    // Search engine
    var search_visible = false;

    $("#search-btn").click(function (event) {
        event.preventDefault();

        if (search_visible) {
            $("#search_zone").slideUp(function () {
                $("#search-btn").find("i").attr("class", "fa fa-search");
                $(this).find("input").val("");
                search_visible = false;
            });
        } else {
            $("#search_zone").slideDown(function () {
                $("#search-btn").find("i")
                    .attr("class", "animated flipInX fa fa-close");
                search_visible = true;
            });
        }
    });

    // Activer dropdown
    $(".dropdown").click(function (event) {
        event.preventDefault();
        if ($(this).hasClass("is-active")) {
            $(this).attr("class", "dropdown");
        } else {
            $(this).attr("class", "dropdown is-active");
        }
    });

    // Datepicker on input
    new DatePicker(document.getElementById('datepicker1'), {
        overlay: true
    });

});