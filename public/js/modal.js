/**
 * @author Mehmet Ozkan
 * @version 1.0
 * @description Simple modal system
 * @file Modal.js 
 */

var Modal = function (node) {
    this.node = node;

    this.open = function () {
        var cross = document.createElement("button");
        var self = this;

        $(cross).attr('type', 'button').attr('class', 'close my-modal-close')
            .attr('aria-label', 'Close').html($("<span></span>")
            .attr('aria-hidden', "true").attr('class', 'fa fa-close'));

        $(this.node).append(cross);
        $("#overlay").fadeIn(function () {
            $(self.node).fadeIn("slow");
        });

        // Close on click "x"
        $(cross).click(function () {
            self.close();
        });
    };

    this.close = function () {
        $(this.node).fadeOut("slow", function () {
            $("#overlay").fadeOut();
        });
    };
};