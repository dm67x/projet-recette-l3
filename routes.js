var express = require('express');
var controllers = require('./controllers');

var routes = {
    "root": controllers.recettes,
    "r": controllers.recettes,
    "u": controllers.utilisateurs,
    "a": controllers.admins,
    "unite": controllers.unites,
    "p": controllers.plannings
}

module.exports = routes;