var express = require('express');
var app = express();
var session = require('express-session');
var expressLayouts = require('express-ejs-layouts');
var bodyParser = require('body-parser');
var models = require('./models');

app.use('/public', express.static(__dirname + '/public'));
app.set('view engine', 'ejs');
app.use(expressLayouts);
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// Session middleware
app.set('trust proxy', 1); // trust first proxy
app.use(session({
	secret: 'keyboard cat',
	resave: false,
	saveUninitialized: true,
	cookie: { secure: 'auto' }
}));

app.use(require('flash')());

var routes = require('./routes');

// Middleware pour avoir acces a la variable req (request) dans mes vues
app.use(function (req, res, next) {
	res.locals.req = req;
	next();
});

// Middleware pour avoir les statistiques dans le footer
app.use(function (req, res, next) {
	models.Utilisateur.count().then(function (cuser) {
		models.Recette.count().then(function (crecette) {
			models.Ingredient.count().then(function (cingr) {
				models.RegimeAlimentaire.findAll().then(function (regimes) {
					res.locals.nb_users = cuser;
					res.locals.nb_recipes = crecette;
					res.locals.regimes = regimes;
					res.locals.nb_ingredients = cingr;
					next();
				})
				.error(function (err) {
					if (err) throw err;
				})
			});
		});
	});
});

// Middleware pour savoir si l'utilisateur est admin
app.use(function (req, res, next) {
	if (req.session.login) {
		models.Utilisateur.find({
			where: {
				login: req.session.login
			}
		})
		.then(function (user) {
			if (user.isAdmin()) req.session.admin = true;
			else req.session.admin = null;
		})
	} else {
		req.session.admin = null;
	}
	next();
});

// Mon middleware pour avoir une architecture MVC
app.use(function (req, res, next) {
	// check req
	// routes/action/params
	var url = req.path;
	var url_split = url.split('/');
	var route = url_split[1] || "root";
	var action = url_split[2] || "index";

	var params = req.params;
	if (req.method == "GET") {
		var params_url = url_split[3];
		if (params_url) { // s'il y a des parametres
			var params_split = params_url.split('&');
			params = {};
			params_split.forEach(param => {
				var p = param.split('=');
				params[p[0]] = p[1];
			});
		}
	}

	if (routes[route] && routes[route][action]) {
		routes[route][action](req, res, next);
	} else {
		next();
	}
});

// Gestion erreurs bidon
// Retour index
app.use(function (req, res, next) {
	res.redirect('/');
});

// Synchronistation base de données et écoute sur le port 3000
models.sequelize.sync().then(function() {
	console.log("synchronisation la base de données");
	app.listen(process.env.PORT || 3000, function () {
		console.log("server running on port 3000");
	});
});